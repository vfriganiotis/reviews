const express = require('express');
const app = express();
const path = require('path')
const port = 3002;
const fs =require('fs');
const cron = require('node-cron');

const pushData = require(path.join(__dirname + '/push.js'))
const scrapData = require(path.join(__dirname + '/reviews.js'))
    
app.get('/',function(req, res){

    res.sendFile(path.join(__dirname + '/client/index.html'));

})

app.get('/scrapData',function(req, res){

    function DONE(){
        res.sendFile(path.join(__dirname + '/client/refresh.html'));
    }
    scrapData( req.query.URL , DONE )

})

app.get('/refresh',function(req, res){


    // const shell = require('./child_helper');
    //
    // const commandList = [
    //     "pm2 restart all"
    // ]
    //
    // shell.series(commandList , (err)=>{
    //     if(err){
    //         console.log(err)
    //     }
    //     console.log('cron run')
    // });

    res.sendFile(path.join(__dirname + '/client/wpfeed.html'));

})

app.get('/pushWPData',function(req, res){
    

    let FirstLang = require('./static/firstLang');
    let SecondLang = require('./static/SecondLang')

    function DONE(){
        res.sendFile(path.join(__dirname + '/client/finish.html'));
    }
    pushData( DONE, req.query.WEBSITE, "e", "a" , FirstLang , SecondLang )

})

app.get('/refreshData',function(req, res){


    const shell = require('./child_helper');

    const commandList = [
        "pm2 restart all"
    ]

    shell.series(commandList , (err)=>{
        if(err){
            console.log(err)
        }
        console.log('cron run')
    });

    res.sendFile(path.join(__dirname + '/client/second.html'));

})

app.get('/safemode',function(req, res){

    res.sendFile(path.join(__dirname + '/client/wpfeed.html'));

})

app.get('/finish', function (req, res) {

    // fs.unlink(path.join(__dirname + '/static/data.json'))
    //
    // fs.unlink(path.join(__dirname + '/static/roomsData.json'))
    fs.stat(path.join(__dirname + '/static/firstLang.json'), function (err, stats) {
        console.log(stats);//here we got all information of file in stats variable

        if (err) {
            return console.error(err);
        }

        fs.unlink(path.join(__dirname + '/static/firstLang.json'),function(err){
            if(err) return console.log(err);
            console.log('file deleted successfully');
        });
    });


    fs.stat(path.join(__dirname + '/static/SecondLang.json'), function (err, stats) {
        console.log(stats);//here we got all information of file in stats variable

        if (err) {
            return console.error(err);
        }

        fs.unlink(path.join(__dirname + '/static/SecondLang.json'),function(err){
            if(err) return console.log(err);
            console.log('file deleted successfully');
        });
    });

    const shell = require('./child_helper');

    const commandList = [
        "pm2 reload all"
    ]

    shell.series(commandList , (err)=>{
        if(err){
            console.log(err)
        }
        console.log('cron run')
    });

    res.sendFile(path.join(__dirname + '/client/done.html'));

})

const server = app.listen(port);
console.log(port)
server.timeout = 0;
