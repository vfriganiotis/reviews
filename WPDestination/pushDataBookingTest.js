const fs =require('fs');

const Request = require('./WPrequest')

// let username = 'testwh.sitesdemo.com';

//WP
async function PushData(DATA,SECOND_DATA,done,website,WPusername,WPpassword){

    // let username = 'testwh.sitesdemo.com';

    let WEBSITE_ID = 0;

    let WPReviews = 'https://www.reviews.sitesdemo.com/wp-json/wp/v2/reviews';

    const WPauth = "Basic " + new Buffer("bill:123").toString("base64");

    let formData = {
        name: website
    };

    let WPWebsitePost = {
        url : 'https://reviews.sitesdemo.com/wp-json/wp/v2/hotels/',
        headers : {
            "Authorization" : WPauth,
            "Content-Type" : "application/x-www-form-urlencoded",
            "Access-Control-Allow-Origin": "*",
            "Accept": "application/json",
            "Content-Disposition" : "attachment;"
        },
        method: "POST",
        formData: formData
    };

    const sv = await Request.init(WPWebsitePost)

    let SECOND_LANGs = require('../WPDestination/hotel');

    let headersAuth = {
        "Authorization" : WPauth,
        "Content-Type" : "application/json",
        "Access-Control-Allow-Origin": "*",
        "Accept": "application/json"
    }


    //THIS IS FOR BOOKING
    for (const item of DATA) {

        const JsonBody = {
            "title" : "Jedi",
            "status" : "publish",
            "websites": [74],
            "hotels": [SECOND_LANGs[0].id],
            "languages": [66]
        }

        JsonBody.title = item.heading;
        JsonBody.content = item.content;
        JsonBody.excerpt = item.excerpt;


        let Sdate = item.date



        //comment for booking
        // let Revdate = item.revdate.split(" ")
        // let RevdateNew = Revdate[Revdate.length - 2] + " " +  Revdate[Revdate.length - 1];


        let newScore = item.score.split(',').join('.')
        JsonBody.fields = {
            link : item.link,
            name : item.name,
            date : Sdate,
            // staydate: RevdateNew,
            score: parseFloat(newScore)
        }

        const WPCreatePost = {
            url : WPReviews,
            headers : headersAuth,
            method: "POST",
            body: JsonBody,
            json: true
        }

        const v = await Request.init(WPCreatePost)

        console.log("eee")

    }

    for (const item of SECOND_DATA) {

        const JsonBody = {
            "title" : "Jedi",
            "status" : "publish",
            "websites": [74],
            "hotels": [SECOND_LANGs[0].id],
            "languages": [67]
        }

        JsonBody.title = item.heading;
        JsonBody.content = item.content;
        JsonBody.excerpt = item.excerpt;

        let Sdate = item.date

        console.log(Sdate)
        // let Revdate = item.revdate.split(" ")
        // let RevdateNew = Revdate[Revdate.length - 2] + " " +  Revdate[Revdate.length - 1];

        let newScore = item.score.split(',').join('.')

        JsonBody.fields = {
            link : item.link,
            name : item.name,
            date : Sdate,
            // staydate: RevdateNew,
            score: parseFloat(newScore)
        }

        const WPCreatePost = {
            url : WPReviews,
            headers : headersAuth,
            method: "POST",
            body: JsonBody,
            json: true
        }

        const v = await Request.init(WPCreatePost)

    }

    console.log('finish')
    done()

}

exports.push = PushData