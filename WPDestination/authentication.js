//require('dotenv').config()

let WPusername = 'bill',
    WPpassword = 123,
    WPauth = "Basic " + new Buffer(WPusername + ":" + WPpassword).toString("base64");

exports.WPHeaders = {
    headers : {
        "Authorization" : WPauth,
        "Content-Type" : "application/json",
        "Access-Control-Allow-Origin": "*",
        "Accept": "application/json"
    },
    WPMediaHeaders : {
        "Authorization": WPauth,
        "Content-Type": "application/x-www-form-urlencoded",
        "Access-Control-Allow-Origin": "*",
        "Accept": "application/json",
        "Content-Disposition": "attachment;"
    }
};
