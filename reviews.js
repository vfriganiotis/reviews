let SCRAPDATA = function(url,done) {
    const puppeteer = require('puppeteer'); // v 1.1.0
    const $ = require('cheerio');
    const passJetbrains = 'R:3.5m3JG&2Sm.L<';
    const fs = require('fs');
    const translate = require('translate');

//const url = 'https://www.tripadvisor.com.gr/Hotel_Review-g189400-d233051-Reviews-The_Stanley-Athens_Attica.html';
//https://www.tripadvisor.com.gr/Hotel_Review-g189400-d233051-Reviews-The_Stanley-Athens_Attica.html
//https://www.tripadvisor.com.gr/Hotel_Review-g663107-d665370-Reviews-Yianna_Hotel-Angistri_Saronic_Gulf_Islands_Attica.html
//https://www.tripadvisor.com.gr/Hotel_Review-g3464181-d1820924-Reviews-Hotel_Agistri-Skala_Angistri_Saronic_Gulf_Islands_Attica.html
//https://www.tripadvisor.com.gr/Hotel_Review-g189400-d677259-Reviews-Hotel_Ariston-Athens_Attica.html
//'https://www.tripadvisor.com.gr/Hotel_Review-g189400-d1510627-Reviews-Athens_Lotus_Hotel-Athens_Attica.html';

    function isEmptyOrSpaces(str){
        return str === null || str.match(/^ *$/) !== null
    }

    let reviewsArrExc = [];
    let reviewsArrCtn = [];
    let REVIEWS = [];
    let PAGECOUNTSexists = 0;
    let pageCount = 6;
    let clicked = true;

    //Trip Advisor Selectors
    let community_content_Card_reg = /hotels-community-tab-common-Card__card--(\S){5}/g;
    let name_reg = /social-member-event-MemberEventOnObjectBlock__member--(\S){5}/g;
    let hotels_hotel_review_req = /location-review-review-list-parts-ExpandableReview__containerStyles--(\S){5}/g;
    let hotels_hotel_Title_req = /location-review-review-list-parts-ReviewTitle__reviewTitle--(\S){5}/g;
    let date_req = /social-member-event-MemberEventOnObjectBlock__event_type--(\S){5}/g;
    let revdate_req = /location-review-review-list-parts-EventDate__event_date--(\S){5}/g;
    let hometown_req =  /social-member-common-MemberHometown__hometown--(\S){5}/g;

    let CONT_CONTENT = '#taplc_resp_hr_ad_wrapper_lower_block_0';

    let pagination_reg = /location-review-pagination-card-PaginationCard__wrapper--(\S){5}/g;

    let content_revie_list_req = /hotels-review-list-parts-ExpandableReview__cta--(\S){5}/g;

    let ReviewCounterLang = 0;

    let MORE_LANGUAGES = /location-review-review-list-parts-LanguageFilter__taLnk--(\S){5}/g;


    function scrapCheeriosContent(context,lang) {

        let hotels_community_content_Card = $(CONT_CONTENT, context).html().match(  community_content_Card_reg  )
        let hotels_name_Member =  $(CONT_CONTENT, context).html().match(  name_reg  )
        let hotels_review =  $(CONT_CONTENT, context).html().match(  hotels_hotel_review_req  )
        let hotels_hotel_Title =  $(CONT_CONTENT, context).html().match(  hotels_hotel_Title_req  )
        let hotels_date =  $(CONT_CONTENT, context).html().match(  date_req  )
        let hotels_revdate = $(CONT_CONTENT, context).html().match( revdate_req   )
        let hotels_hometown = $(CONT_CONTENT, context).html().match( hometown_req )

        if( hotels_hometown === null ){
            hotels_hometown = 0
        }

        if( hotels_community_content_Card !== null){
            console.log("eimai to hotels_community_content_Card  " + hotels_community_content_Card.length)
        }else{
            console.log("eimai to hotels_community_content_Card kai  eimai null")
        }

        if( hotels_review !== null){
            console.log("eimai to hotels_review "  + hotels_review.length)
        }else{
            console.log("eimai to hotels_review kai einai null")
        }

        if( hotels_name_Member !== null){
            console.log("eimai to  hotels_name_Member "  + hotels_name_Member.length)
        }else{
            console.log("eimai to hotels_name_Member kai einai null")
        }

        if( hotels_hotel_Title !== null){
            console.log("eimai to hotels_hotel_Title "  + hotels_hotel_Title.length)
        }else{
            console.log("eimai to hotels_hotel_Title kai einai null")
        }

        if( hotels_date !== null){
            console.log("eimai to hotels_date "  + hotels_date.length)
        }else{
            console.log("eimai to hotels_date kai einai null")
        }

        if( hotels_hometown !== null){
            console.log("eimai to hotels_hometown "  + hotels_hometown.length)
        }else{
            console.log("eimai to hotels_hometown kai einai null")
        }

        if( hotels_revdate !== null){
            console.log("eimai to hotels_revdate "  + hotels_revdate.length)
        }else{
            console.log("eimai to hotels_revdate kai einai null")
        }


        if(hotels_name_Member !== null && hotels_review !== null){
            $( "." + hotels_community_content_Card[0] , context).each(function () {

                let review = {
                    name: "",
                    heading: "",
                    content: "",
                    excerpt: ""
                }

                let reviewText = $(this).find("." + hotels_review[0] + " q").text()

                let dd = $(this).find("." + hotels_review[0]).length

                let name = "";
                let hometown = " ";
                let revdate = " ";
                let date = " ";
                let heading = " ";
                let link = "";

                if( !( hotels_name_Member == null) ){
                    name = $(this).find("." + hotels_name_Member[0]).text()
                }

                if( !(hotels_hometown == null) ){
                    hometown = $(this).find("." + hotels_hometown[0]).text()
                }

                if( !(hotels_revdate == null) ){
                    revdate = $(this).find("." + hotels_revdate[0]).text()
                }

                if( !(hotels_date == null) ){
                    date = $(this).find("." + hotels_date[0]).text()
                }

                if( !(hotels_hotel_Title == null) ){
                    heading = $(this).find("." + hotels_hotel_Title[0]).text();
                    link    = $(this).find("." + hotels_hotel_Title[0] + " a").attr('href')
                }

                if (dd > 0) {
                    review.name = name;
                    review.heading = heading;
                    review.content = reviewText;
                    review.link = link;
                    review.lang = lang;
                    review.date = date;
                    review.revdate = revdate;
                    review.hometown = hometown;
                    review.review_from = 'TRIP ADVISOR';
                    reviewsArrCtn.push(review)
                }
                console.log("review")
                //console.log(review)

            });
        }

    }

    async function init(page, scrapCheeriosContent, Lang , lang) {

        reviewsArrExc = [];
        reviewsArrCtn = [];
        REVIEWS = [];
        PAGECOUNTSexists = 0;
        pageCount = 6;

        await page.evaluate(() => {
            scroll(0, 99999)
        });

        await page.waitFor(100);

        let bodyHTMLpages = await page.evaluate(() => document.body.innerHTML);

        let hotels_community_content_Card

        await page.waitFor(5000);

        if($( CONT_CONTENT , bodyHTMLpages ).html().match( pagination_reg ) !== null){
            hotels_community_content_Card = $( CONT_CONTENT , bodyHTMLpages ).html().match( pagination_reg )
        }

        if(hotels_community_content_Card !== undefined){
            if ($("." + hotels_community_content_Card[0] , bodyHTMLpages).first().length) {

                PAGECOUNTSexists = $("." + hotels_community_content_Card[0] , bodyHTMLpages).first().find('.pageNum').length
                console.log("sasas " + PAGECOUNTSexists)

            }
        }

        console.log("tosa page counts uparxoun 1 " + PAGECOUNTSexists)
        await page.evaluate(() => {
            scroll(0, 99999)
        });

        await page.waitFor(100);

        if(hotels_community_content_Card !== undefined){
            if ($("." + hotels_community_content_Card[0] , bodyHTMLpages).first().length) {

                PAGECOUNTSexists = $("." + hotels_community_content_Card[0] , bodyHTMLpages).first().find('.pageNum').length

                console.log("sasas " + PAGECOUNTSexists)
            }
        }

        console.log("tosa page counts uparxoun 2 " + PAGECOUNTSexists)
        if (PAGECOUNTSexists <= pageCount) {
            if (PAGECOUNTSexists === 0) {
                pageCount = 1
            } else {
                pageCount = PAGECOUNTSexists
            }
        }


        console.log("tosa page counts uparxoun s " + pageCount)
        for (let i = 0; i < pageCount; i++) {

            let bodyHTMLex = await page.evaluate(() => document.body.innerHTML);

            if ($('#autoTranslateNo', bodyHTMLex).length && clicked) {

                await page.$$eval('#autoTranslateNo', function (elements) {
                    elements[0].click()
                });
                clicked = false;

            }

            console.log($('.next', bodyHTMLex).first().not('.disabled').length)
            if (clicked && $('.next', bodyHTMLex).first().not('.disabled').length ) {

                await page.$$eval('.ui_pagination a.nav.next', function (elements) {
                    elements[0].click()
                });

            }

            await page.waitFor(100);
            console.log("EPOMONI SELIDA >>>")
        }

        console.log(clicked)
        console.log(pageCount)

        for (let i = 0; i < pageCount; i++) {

            let bodyHTMLex = await page.evaluate(() => document.body.innerHTML);

            let content_revie_list = $( CONT_CONTENT , bodyHTMLex ).html().match( content_revie_list_req )

            if( content_revie_list !== null){
                await page.$$eval("." + content_revie_list[0] , function (elements) {
                    elements[0].click()
                });
            }

            await page.waitFor(100);

            let bodyHTMLCtn = await page.evaluate(() => document.body.innerHTML);

            scrapCheeriosContent(bodyHTMLCtn, lang)

            let bodyHTMLexButtonPrev = await page.evaluate(() => document.body.innerHTML);

            if( $('a.previous', bodyHTMLexButtonPrev).first().not('.disabled') !== undefined ){
                if (clicked && $('a.previous', bodyHTMLexButtonPrev).first().not('.disabled').length) {

                    console.log("kapou edw1")
                    await page.$$eval('a.previous', function (elements) {
                        elements[0].click()
                    });

                    await page.waitFor(100);

                }else{
                    console.log("den mpika pote")
                }
            }

            let bodyHTMLexButtonNext = await page.evaluate(() => document.body.innerHTML);

            if($('a.next', bodyHTMLex).not('.disabled') !== undefined){
                if (!clicked && $('a.next', bodyHTMLex).not('.disabled').length ) {

                    await page.$$eval('.next', function (elements) {
                        elements[0].click()
                    });
                    await page.waitFor(100);
                    console.log("EPOMONI SELIDA >>")
                }
            }

            console.log('test1')

        }

        let bodyHTMLex = await page.evaluate(() => document.body.innerHTML);

        let content_revie_list = $( CONT_CONTENT , bodyHTMLex ).html().match( content_revie_list_req )

        if( content_revie_list !== null){
            await page.$$eval("." + content_revie_list[0] , function (elements) {

                console.log("GIA NA DOUME")
                elements[0].click()
                console.log("GIA NA DOUME1")

            });
        }

        let bodyHTMLCtn = await page.evaluate(() => document.body.innerHTML);

        scrapCheeriosContent(bodyHTMLCtn, lang)

        console.log("LANG == " + ReviewCounterLang)
        let translateFirst = [];
        for( let i =0; i < reviewsArrCtn.length; i++ ){
            let rev = {
                name: reviewsArrCtn[i].name,
                heading: reviewsArrCtn[i].heading,
                content: reviewsArrCtn[i].content,
                excerpt: reviewsArrCtn[i].excerpt,
                lang: reviewsArrCtn[i].lang,
                link: reviewsArrCtn[i].link,
                date: reviewsArrCtn[i].date,
                revdate: reviewsArrCtn[i].revdate,
                review_from: 'TRIP ADVISOR'
                // hometown: fooa
            }
            translate.from = 'el';
            translateFirst.push(rev)

        }

        ReviewCounterLang++

        let RT =  translateFirst.reduce(function(RevArr,obj,index){

            console.log('111111')
            console.log(index)
            let getIn = true;

            if(index === 0){
                //RevArr.push(obj)
            }else{
                for( let i = 0; i < RevArr.length; i++  ){
                    if( RevArr[i].name === obj.name && RevArr[i].heading === obj.heading){
                        getIn = false;
                    }
                }
            }

            console.log(getIn)

            if(getIn){
                RevArr.push(obj)
            }

            return RevArr

        },[])

        fs.writeFile('./static/' + Lang + '.json', JSON.stringify(RT), 'utf8', (err) => {
            console.log("change data output")
            return (err) ? console.log(err) : console.log("The file pages was saved!");
        });

    }

    async function scrap() {

        //const browser = await puppeteer.launch({headless: true , args: ['--no-sandbox', '--disable-setuid-sandbox']} );
        const browser = await puppeteer.launch({headless: false});
        const page = await browser.newPage();
        // await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

        console.log("b4 page load")

        await page.goto(url ,  {waitUntil: 'load', timeout: 0} );

        console.log("after page load")
        await page.evaluate(() => {
            scroll(0, 99999)
        });
        await page.waitFor(100);
        console.log("after page load and evaluation")

        let bodyHTMLStarButton = await page.evaluate(() => document.body.innerHTML);

        if ($('#ReviewRatingFilter_5', bodyHTMLStarButton).length > 0) {
            await page.$$eval('#ReviewRatingFilter_5', function (elements) {
                elements[0].click()
            });
        }
        await page.waitFor(100);

        if (await page.$('input[value="en"]') !== null) {
            console.log('Engish Lang Find')
            await page.$$eval('input[value="en"]', function (elements) {
                elements[0].click()
            });
        }else{
            await page.$$eval('#LanguageFilter_1', function (elements) {
                elements[0].click()
            });
            console.log('kaname Click tin prwth glwssa')
        }

        console.log('awawww')

        await page.waitFor(5000);

        await page.$$eval('footer > div > div > div > div > div:nth-child(2) [role="button"]', function (elements) {
            elements[0].click()
        });
        await page.waitFor(5000);


        console.log('aw1')
        await page.$$eval('body [hreflang="en-GB"]', function (elements) {
            elements[0].click()
        });

        await page.waitFor(5000);
        await init(page, scrapCheeriosContent, 'firstLang' , 1)

        await page.waitFor(100);
        let bodyHTMLcheckLangButtonSec = await page.evaluate(() => document.body.innerHTML);

        let hotels_community_content_Card = $(CONT_CONTENT, bodyHTMLcheckLangButtonSec).html().match( MORE_LANGUAGES )

        console.log('wwwwww')
        console.log( hotels_community_content_Card)

        if ( hotels_community_content_Card !== null && hotels_community_content_Card.length > 0) {

            console.log('.' + hotels_community_content_Card[0])
            await page.$$eval( '.' + hotels_community_content_Card[0]  , function (elements) {
                elements[0].click()
            });

            await page.waitFor(100);

            if (await page.$('input[value="el"]') !== null) {
                console.log('Greek Lang Find')

                await page.$$eval('input[value="el"]', function (elements) {
                    console.log('to kaname click e')
                    elements[0].click()
                });

            }else{
                console.log('NO greek Greek Lang Find')
                await page.$$eval('#LanguageFilter_2', function (elements) {
                    elements[0].click()
                });
            }

        }else{
            await page.$$eval('#LanguageFilter_2', function (elements) {
                elements[0].click()
            });
        }

        await page.evaluate(() => {
            scroll(0, 99999)
        });

        console.log('ea')

        await page.waitFor(3000);

        await page.$$eval('footer > div > div > div > div >  div:nth-child(2) [role="button"]', function (elements) {
            elements[0].click()
        });
        await page.waitFor(5000);


        await page.$$eval('body [hreflang="el"]', function (elements) {
            elements[0].click()
        });

        await page.waitFor(5000);

        await init(page, scrapCheeriosContent, 'SecondLang' , 2)
        await browser.close()

        done()
    }

    scrap()

}

module.exports = SCRAPDATA
