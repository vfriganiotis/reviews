const puppeteer = require('puppeteer'); // v 1.1.0
const $ = require('cheerio');
const passJetbrains = 'R:3.5m3JG&2Sm.L<';
const fs = require('fs');
const translate = require('translate');

//const url = 'https://www.tripadvisor.com.gr/Hotel_Review-g189400-d233051-Reviews-The_Stanley-Athens_Attica.html';
//https://www.tripadvisor.com.gr/Hotel_Review-g189400-d233051-Reviews-The_Stanley-Athens_Attica.html
//https://www.tripadvisor.com.gr/Hotel_Review-g663107-d665370-Reviews-Yianna_Hotel-Angistri_Saronic_Gulf_Islands_Attica.html
//https://www.tripadvisor.com.gr/Hotel_Review-g3464181-d1820924-Reviews-Hotel_Agistri-Skala_Angistri_Saronic_Gulf_Islands_Attica.html
//https://www.tripadvisor.com.gr/Hotel_Review-g189400-d677259-Reviews-Hotel_Ariston-Athens_Attica.html
//'https://www.tripadvisor.com.gr/Hotel_Review-g189400-d1510627-Reviews-Athens_Lotus_Hotel-Athens_Attica.html';

let reviewsArrExc = [];
let reviewsArrCtn = [];
let REVIEWS = [];
let PAGECOUNTSexists = 0;
let pageCount = 6;
let clicked = true;

function scrapCheeriosExcerpt(context,lang) {
    $('.hotels-hotel-review-community-content-Card__card--2dqMT', context).each(function () {

        let review = {
            name: "",
            heading: "",
            content: "",
            excerpt: ""
        }

        let name = $(this).find('.social-member-MemberEventOnObjectBlock__member--21vv3').text()
        let reviewText = $(this).find('q.hotels-hotel-review-community-content-review-list-parts-ExpandableReview__reviewText--1OjOL').text()
        let heading = $(this).find('.hotels-hotel-review-community-content-review-list-parts-ReviewTitle__reviewTitleText--2vGeO').text()
        let link = $(this).find('.hotels-hotel-review-community-content-review-list-parts-ReviewTitle__reviewTitleText--2vGeO').attr('href')
        let date = $(this).find('.social-member-MemberEventOnObjectBlock__event_type--E3vHh').text()
        let revdate = $(this).find('.hotels-review-list-parts-EventDate__event_date--1agCM').text()
        let hometown = $(this).find('.social-member-MemberHometown__hometown--3WpTM').text()

        if ($(this).find('q.hotels-hotel-review-community-content-review-list-parts-ExpandableReview__reviewText--1OjOL').length > 0) {
            review.name = name;
            review.heading = heading;
            review.excerpt = reviewText;
            review.link = link;
            review.lang = lang;
            review.date = date;
            review.revdate = revdate;
            review.hometown = hometown;

            reviewsArrExc.push(review)
        }

    });
}

function scrapCheeriosContent(context,lang) {
    $('.hotels-hotel-review-community-content-Card__card--2dqMT', context).each(function () {

        let review = {
            name: "",
            heading: "",
            content: "",
            excerpt: ""
        }

        let name = $(this).find('.social-member-MemberEventOnObjectBlock__member--21vv3').text()
        let reviewText = $(this).find('q.hotels-hotel-review-community-content-review-list-parts-ExpandableReview__reviewText--1OjOL').text()
        let heading = $(this).find('.hotels-hotel-review-community-content-review-list-parts-ReviewTitle__reviewTitleText--2vGeO').text()
        let link = $(this).find('.hotels-hotel-review-community-content-review-list-parts-ReviewTitle__reviewTitleText--2vGeO').attr('href')

        let dd = $(this).find('q.hotels-hotel-review-community-content-review-list-parts-ExpandableReview__reviewText--1OjOL').length

        if (dd > 0) {
            review.name = name;
            review.heading = heading;
            review.content = reviewText;
            review.link = link;
            review.lang = lang;
            reviewsArrCtn.push(review)
        }

    });
}

async function init(page, scrapCheeriosExcerpt, scrapCheeriosContent, Lang , lang) {

    reviewsArrExc = [];
    reviewsArrCtn = [];
    REVIEWS = [];
    PAGECOUNTSexists = 0;
    pageCount = 6;

    await page.evaluate(() => {
        scroll(0, 99999)
    });

    await page.waitFor(5000);


    let bodyHTMLpages = await page.evaluate(() => document.body.innerHTML);

    if ($('.hotels-hotel-review-layout-Section__joinable--STV_u', bodyHTMLpages).length) {

        PAGECOUNTSexists = $('.hotels-hotel-review-layout-Section__joinable--STV_u', bodyHTMLpages).next('div').find('.pageNumbers .pageNum').length

    }

    if (PAGECOUNTSexists <= pageCount) {
        if (PAGECOUNTSexists === 0) {
            pageCount = 1
        } else {
            pageCount = PAGECOUNTSexists
        }
    }

    for (let i = 0; i < pageCount; i++) {

        let bodyHTMLex = await page.evaluate(() => document.body.innerHTML);

        if ($('#autoTranslateNo', bodyHTMLex).length && clicked) {

            await page.$$eval('#autoTranslateNo', function (elements) {

                elements[0].click()

            });
            clicked = false;

        }

        if (clicked && $('.next.primary_dark', bodyHTMLex).length) {

            await page.$$eval('.next.primary_dark', function (elements) {
                elements[0].click()
            });

        }

        await page.waitFor(2000);
        console.log("EPOMONI SELIDA >>")
    }

    console.log(clicked)

    for (let i = 0; i < pageCount; i++) {

        let bodyHTMLexButtonPrev = await page.evaluate(() => document.body.innerHTML);

        if (clicked && $('.previous.secondary_dark', bodyHTMLexButtonPrev).length) {

            await page.$$eval('.previous.secondary_dark', function (elements) {
                elements[0].click()
            });

            await page.waitFor(2000);
            console.log("EPOMONI SELIDA >>")

        }

        let bodyHTMLex = await page.evaluate(() => document.body.innerHTML);

        scrapCheeriosExcerpt(bodyHTMLex, lang)

        await page.$$eval('.hotels-hotel-review-community-content-review-list-parts-ExpandableReview__cta--3_zOW', function (elements) {

            elements[0].click()

        });

        await page.waitFor(200);

        let bodyHTMLCtn = await page.evaluate(() => document.body.innerHTML);

        scrapCheeriosContent(bodyHTMLCtn, lang)

        let bodyHTMLexButtonNext = await page.evaluate(() => document.body.innerHTML);

        if (!clicked && $('.next.primary_dark', bodyHTMLexButtonNext).length) {

            await page.$$eval('.next.primary_dark', function (elements) {
                elements[0].click()
            });
            await page.waitFor(2000);
            console.log("EPOMONI SELIDA >>")
        }

    }

    let counter = 0;
    for (let i = 0; i < reviewsArrExc.length; i++) {

        for (let y = 0; y < reviewsArrCtn.length; y++) {

            if (reviewsArrExc[i].name === reviewsArrCtn[y].name && reviewsArrExc[i].heading === reviewsArrCtn[y].heading) {


                let rev = {
                    name: reviewsArrCtn[y].name,
                    heading: reviewsArrCtn[y].heading,
                    content: reviewsArrCtn[y].content,
                    excerpt: reviewsArrExc[y].excerpt,
                    lang: reviewsArrExc[y].lang,
                    link: reviewsArrExc[y].link,
                    date: reviewsArrExc[y].date,
                    revdate: reviewsArrExc[y].revdate,
                    // hometown: fooa
                }


                translate.from = 'el';
                let fooa = await translate( reviewsArrExc[y].hometown , { to: 'en', engine: 'google', key: 'AIzaSyCP6JXuY_k7qGsoSYDZm76m4FFpYv8Q-8Y' }).then(text => {
                    console.log("------")
                    console.log(text);  // Hola mundo

                    rev.hometown = text
                    console.log("------")
                });


                console.log(reviewsArrExc[y].hometown)
                console.log("to en")
                console.log(fooa)


                console.log("to vrikame " + counter)
                REVIEWS.push(rev)
                counter++
            }

        }

    }

    //const fooa = await translate('Hello world', 'el');

    fs.writeFile('./static/' + Lang + '.json', JSON.stringify(REVIEWS), 'utf8', (err) => {
        console.log("change data output")
        return (err) ? console.log(err) : console.log("The file pages was saved!");
    });

}

async function scrap() {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    await page.goto(url);

    await page.evaluate(() => {
        scroll(0, 99999)
    });
    await page.waitFor(5000);

    let bodyHTMLcheckLangButton = await page.evaluate(() => document.body.innerHTML);

    if ($('#LanguageFilter_1', bodyHTMLcheckLangButton).length > 0) {
        await page.$$eval('#LanguageFilter_1', function (elements) {
            elements[0].click()
        });
    }

    let bodyHTMLStarButton = await page.evaluate(() => document.body.innerHTML);

    if ($('#ReviewRatingFilter_5', bodyHTMLStarButton).length > 0) {
        await page.$$eval('#ReviewRatingFilter_5', function (elements) {
            elements[0].click()
        });
    }

    await page.evaluate(() => {
        scroll(0, 99999)
    });
    await page.waitFor(5000);

    await init(page, scrapCheeriosExcerpt, scrapCheeriosContent, 'firstLang' , 1)

    console.log("111")

    let bodyHTMLcheckLangButtonSec = await page.evaluate(() => document.body.innerHTML);

    if ($('#LanguageFilter_2', bodyHTMLcheckLangButtonSec).length > 0) {
        await page.$$eval('#LanguageFilter_2', function (elements) {
            elements[0].click()
        });
    }

    await init(page, scrapCheeriosExcerpt, scrapCheeriosContent, 'SecondLang' , 2)
    console.log("11112")
    await browser.close()

}

scrap()