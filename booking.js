let SCRAPDATABOOKING = function(url,done) {
    const puppeteer = require('puppeteer'); // v 1.1.0
    const $ = require('cheerio');
    const fs = require('fs');

    let firstLangArr = []
    let reviewsArrCtn = []
    let pageCount = 6;
    let counter = 0;
    function scrapCheeriosContent(context,lang) {

        $( ".review_list_new_item_block" , context).each(function () {

            let review = {}

            let name = $(this).find(".bui-avatar-block__title").text()
            let country = $(this).find(".bui-avatar-block__subtitle").text().replace(/\n/g, '').replace(/\s/g,'')
            let date = $(this).find(".c-review-block__date").text().replace(/\n/g, ' ')
            let heading = $(this).find(".c-review-block__title").text().replace(/\n/g, '')
            let reviewText = $(this).find(".c-review__body").first().text()
            let reviewScore = $(this).find(".bui-review-score__badge").text().replace(/\s/g,'')

            //let link = $(this).find("." + hotels_hotel_Title[0]).attr('href')

            review.name = name;
            review.heading = heading;
            review.content = reviewText;
            review.lang = lang;
            review.date = date;
            review.country = country;
            review.counter = counter;
            review.score = reviewScore;

            counter++
            if(  parseFloat(reviewScore) > 7){
                reviewsArrCtn.push(review)
            }

        });
    }

    async function scrap() {
        const browser = await puppeteer.launch({headless: false});
        const page = await browser.newPage();
        await page.goto(url);

        await page.evaluate(() => {
            scroll(0, 99999)
        });
        await page.waitFor(1000);

        let bodyHTMLcheckLangButton = await page.evaluate(() => document.body.innerHTML);

        if ($('.hp_nav_reviews_link.toggle_review', bodyHTMLcheckLangButton).length > 0) {
            await page.$$eval('.hp_nav_reviews_link.toggle_review', function (elements) {
                elements[0].click()
            });
        }

        await page.evaluate(() => {
            scroll(0, 99999)
        });

        await page.waitFor(1000);

        let  bodyHTMLcheckLangButtonEn = await page.evaluate(() => document.body.innerHTML);

        if ($('input.reviews_filters_language_main', bodyHTMLcheckLangButtonEn).length > 0) {

            await page.$$eval('input.reviews_filters_language_main[value="en"]', function (elements) {
                elements[0].click()
            });

        }

        let  bodyHTMLcheckReviews = await page.evaluate(() => document.body.innerHTML);
        scrapCheeriosContent( bodyHTMLcheckReviews , 1 )

        for( let i = 0; i < pageCount; i++){

            let htmlNew = await page.evaluate(() => document.body.innerHTML);
            console.log($('.bui-pagination__item.bui-pagination__next-arrow', htmlNew).not(".bui-pagination__item--disabled").length)

            if ($('.bui-pagination__item.bui-pagination__next-arrow', htmlNew).not(".bui-pagination__item--disabled").length > 0) {

                console.log("ksekinaw")


                await page.$$eval('.bui-pagination__item.bui-pagination__next-arrow .pagenext', function (elements) {
                    elements[0].click()
                });

                scrapCheeriosContent( htmlNew , 1 )

                await page.evaluate(() => {
                    scroll(0, 99999)
                });

                await page.waitFor(1000);
            }

        }

        await page.evaluate(() => {
            scroll(0, 99999)
        });

        await page.waitFor(1000);


        console.log(reviewsArrCtn)

        fs.writeFile('./static/booking/firstLang.json', JSON.stringify(reviewsArrCtn), 'utf8', (err) => {
            console.log("change data output")
            return (err) ? console.log(err) : console.log("The file pages was saved!");
        });


        reviewsArrCtn = [];

        let  bodyHTMLcheckLangButtonGreece = await page.evaluate(() => document.body.innerHTML);

        if ($('input.reviews_filters_language_main', bodyHTMLcheckLangButtonGreece).length > 0) {

            await page.$$eval('#reviews_filters_lang_modal_button', function (element) {
                console.log(element)
                element[0].click()
            });

            await page.evaluate(() => {
                scroll(0, 99999)
            });

            await page.waitFor(4000);

            if (await page.$('input[value="el"]') !== null) {
                console.log('Greek Lang Find')
                await page.$$eval('input.language_filter_checkbox[value="el"]', function (elements) {
                    elements[0].click()
                });
            }else{
                console.log('No Greek Lang Find')
                var newRTSQLbs = []
                fs.writeFile('./hotelier/booking/secondLang.json', JSON.stringify(newRTSQLbs) , 'utf8', (err) => {
                    console.log("change data output")
                    return (err) ? console.log(err) : console.log("The file pages was saved!");
                });
                await browser.close()
                done()
            }

        }

        await page.evaluate(() => {
            scroll(0, 99999)
        });

        await page.waitFor(1000);

        let  bodyHTMLcheckReviewsSecond = await page.evaluate(() => document.body.innerHTML);
        scrapCheeriosContent( bodyHTMLcheckReviewsSecond , 2 )

        for( let i = 0; i < pageCount; i++){

            let htmlNew = await page.evaluate(() => document.body.innerHTML);
            console.log($('.bui-pagination__item.bui-pagination__next-arrow', htmlNew).not(".bui-pagination__item--disabled").length)

            if ($('.bui-pagination__item.bui-pagination__next-arrow', htmlNew).not(".bui-pagination__item--disabled").length > 0) {

                console.log("ksekinaw")

                await page.$$eval('.bui-pagination__item.bui-pagination__next-arrow .pagenext', function (elements) {
                    elements[0].click()
                });

                scrapCheeriosContent( htmlNew , 2 )

                await page.evaluate(() => {
                    scroll(0, 99999)
                });

                await page.waitFor(1000);
            }

        }

        await page.evaluate(() => {
            scroll(0, 99999)
        });

        await page.waitFor(1000);

        console.log(reviewsArrCtn)

        fs.writeFile('./static/booking/secondLang.json', JSON.stringify(reviewsArrCtn), 'utf8', (err) => {
            console.log("change data output")
            return (err) ? console.log(err) : console.log("The file pages was saved!");
        });


        await browser.close()

        done()
    }

    scrap()

}

module.exports = SCRAPDATABOOKING

